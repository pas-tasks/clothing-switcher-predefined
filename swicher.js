// legend
// T = top
// B = bottom
// P = print
// B = belt

// initial image
const INITIAL_IMAGE = 'https://via.placeholder.com/350?text=Initial';
// top one first layer combinations [TOP/BOTTOM]
const T1_B1_IMAGE = 'https://via.placeholder.com/350?text=T1-B1';
const T1_B2_IMAGE = 'https://via.placeholder.com/350?text=T1-B2';
const T1_B3_IMAGE = 'https://via.placeholder.com/350?text=T1-B3';
// top one second layer combinations [TOP/BOTTOM/PRINT]
const T1_B1_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B1-P1';
const T1_B1_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B1-P2';
const T1_B1_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B1-P3';
const T1_B2_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B2-P1';
const T1_B2_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B2-P2';
const T1_B2_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B2-P3';
const T1_B3_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B3-P1';
const T1_B3_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B3-P2';
const T1_B3_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B3-P3';
// top one third layer combinations [TOP/BOTTOM/PRINT/BELT]
const T1_B1_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B1-P1-BG';
const T1_B1_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B1-P1-BB';
const T1_B1_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B1-P2-BG';
const T1_B1_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B1-P2-BB';
const T1_B1_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B1-P3-BG';
const T1_B1_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B1-P3-BB';
const T1_B3_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B3-P1-BB';
const T1_B3_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B3-P1-BG';
const T1_B3_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B3-P2-BG';
const T1_B3_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B3-P2-BB';
const T1_B3_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B3-P3-BG';
const T1_B3_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B3-P3-BB';
const T1_B2_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B2-P1-BG';
const T1_B2_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T1-B2-P1-BB';
const T1_B2_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B2-P2-BB';
const T1_B2_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T1-B2-P2-BG';
const T1_B2_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B2-P3-BB';
const T1_B2_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T1-B2-P3-BG';

// top two first layer conbinations [TOP/BOTTOM]
const T2_B1_IMAGE = 'https://via.placeholder.com/350?text=T2-B1';
const T2_B2_IMAGE = 'https://via.placeholder.com/350?text=T2-B2';
const T2_B3_IMAGE = 'https://via.placeholder.com/350?text=T2-B3';
// top two second layer combinations [TOP/BOTTOM/PRINT]
const T2_B1_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B1-P1';
const T2_B1_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B1-P2';
const T2_B1_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B1-P3';
const T2_B2_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B2-P1';
const T2_B2_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B2-P2';
const T2_B2_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B2-P3';
const T2_B3_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B3-P1';
const T2_B3_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B3-P2';
const T2_B3_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B3-P3';
// top two second layer combinations [TOP/BOTTOM/PRINT/BELT]
const T2_B1_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B1-P1-BB';
const T2_B1_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B1-P1-BG';
const T2_B1_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B1-P3-BB';
const T2_B1_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B1-P3-BG';
const T2_B1_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B1-P2-BB';
const T2_B1_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B1-P2-BG';
const T2_B3_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B3-P1-BB';
const T2_B3_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B3-P1-BG';
const T2_B3_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B3-P3-BB';
const T2_B3_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B3-P3-BG';
const T2_B3_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B3-P2-BB';
const T2_B3_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B3-P2-BG';
const T2_B2_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B2-P1-BB';
const T2_B2_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T2-B2-P1-BG';
const T2_B2_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B2-P3-BB';
const T2_B2_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T2-B2-P3-BG';
const T2_B2_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B2-P2-BB';
const T2_B2_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T2-B2-P2-BG';

// top three first layer combinations [TOP/BOTTOM]
const T3_B1_IMAGE = 'https://via.placeholder.com/350?text=T3-B1';
const T3_B2_IMAGE = 'https://via.placeholder.com/350?text=T3-B2';
const T3_B3_IMAGE = 'https://via.placeholder.com/350?text=T3-B3';
// top three second layer combinations [TOP/BOTTOM/PRINT]
const T3_B1_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B1-P1';
const T3_B1_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B1-P2';
const T3_B1_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B1-P3';
const T3_B2_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B2-P1';
const T3_B2_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B2-P2';
const T3_B2_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B2-P3';
const T3_B3_P1_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B3-P1';
const T3_B3_P2_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B3-P2';
const T3_B3_P3_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B3-P3';
// top three third layer combinations [TOP/BOTTOM/PRINT/BELT]
const T3_B1_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B1-P1-BB';
const T3_B1_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B1-P1-BG';
const T3_B1_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B1-P3-BB';
const T3_B1_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B1-P3-BG';
const T3_B1_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B1-P2-BB';
const T3_B1_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B1-P2-BG';
const T3_B3_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B3-P1-BG';
const T3_B3_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B3-P1-BB';
const T3_B3_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B3-P3-BB';
const T3_B3_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B3-P3-BG';
const T3_B3_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B3-P2-BB';
const T3_B3_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B3-P2-BG';
const T3_B2_P1_BR_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B2-P1-BB';
const T3_B2_P1_BG_IMAGE = 'https://via.placeholder.com/350/85bf31/FFF?text=T3-B2-P1-BG';
const T3_B2_P3_BR_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B2-P3-BB';
const T3_B2_P3_BG_IMAGE = 'https://via.placeholder.com/350/0693e3/FFF?text=T3-B2-P3-BG';
const T3_B2_P2_BR_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B2-P2-BB';
const T3_B2_P2_BG_IMAGE = 'https://via.placeholder.com/350/83af99/FFF?text=T3-B2-P2-BG';

// Using an object literal for a jQuery feature
var clothSwitcher = {

    init: function( settings ) {
        clothSwitcher.config = {
            currentTopStyle: null,
            currentBottomStyle: null,
            currentBeltStyle: null,
            currentPrintStyle: null,

            // step one
            topSelector: $('.jcarousel-wrapper .jcarousel ul#topStyle li'),
            topNthSelector: $('.jcarousel-wrapper .jcarousel ul#topStyle li:nth-child(2)'),

            bottomSelector: $('.jcarousel-wrapper .jcarousel ul#bottomStyle li'),
            bottomNthSelector: $('.jcarousel-wrapper .jcarousel ul#bottomStyle li:nth-child(2)'),

            // step two
            printSelector: $('ul.printStype li'),

            // step three
            colorSelector: $('ul.colorStyle li'),

            // steps action
            actionButton: $('.switcher-actions-next'),
            stepsContainer: $('.steps'),

            // preview
            basePreviewImage: $('.swicher-preview img#base'),
            selectedPreviewImage: $('.swicher-preview img#selected'),

            jcarouselTopControl: $('#top a')
        };
 
        // Allow overriding the default config
        $.extend( clothSwitcher.config, settings );
 
        clothSwitcher.setup();
    },

    setup: function() {
        // step 1 - top
        clothSwitcher.config.topSelector.click( clothSwitcher.topSelectorClicked );
        clothSwitcher.config.topSelector.hover( clothSwitcher.topOnHoverIn, clothSwitcher.onHoverOut);

        // step 1 - bottom
        clothSwitcher.config.bottomSelector.click( clothSwitcher.bottomSelectorClicked );
        clothSwitcher.config.bottomSelector.hover( clothSwitcher.bottomOnHoverIn, clothSwitcher.onHoverOut);

        // step 2
        clothSwitcher.config.printSelector.click( clothSwitcher.printSelectorClicked );
        clothSwitcher.config.printSelector.hover( clothSwitcher.printOnHoverIn, clothSwitcher.onHoverOut);

        // step 3
        clothSwitcher.config.colorSelector.click( clothSwitcher.beltColorSelectorClicked );
        clothSwitcher.config.colorSelector.hover( clothSwitcher.beltOnHoverIn, clothSwitcher.onHoverOut);

        clothSwitcher.config.actionButton.on('click', function(e) {
            clothSwitcher.config.stepsContainer.removeClass('active');
            $('#step-' + $(this).attr('id')).addClass('active');
            if ($(this).attr('id') === '01') {
                clothSwitcher.setDefault();
            }
            if ($(this).attr('id') === '04') {
                clothSwitcher.config.actionButton.prop('disabled', false);
            } else {
                clothSwitcher.config.actionButton.prop('disabled', true);
            }
        });

        // jcarousel bind event
        $(document).on('click', '#top a.jcarousel-control-next', function() {
            clothSwitcher.onTopControlTriger($(this), 'next');
        });

        $(document).on('click', '#top a.jcarousel-control-prev', function() {
            clothSwitcher.onTopControlTriger($(this), 'prev');
        });

        $(document).on('click', '#bottom a.jcarousel-control-next', function() {
            clothSwitcher.onBottomControlTriger($(this), 'next');
        });

        $(document).on('click', '#bottom a.jcarousel-control-prev', function() {
            clothSwitcher.onBottomControlTriger($(this), 'prev');
        });

        clothSwitcher.setDefault();
    },
    
    onTopControlTriger(self, control) {
        if (self.hasClass('inactive')) {
            if (control == 'prev') {
                clothSwitcher.config.topSelector.first().trigger('mouseenter');
            } else {
                clothSwitcher.config.topSelector.last().trigger('mouseenter');
            }
        } else {
            clothSwitcher.config.topNthSelector.trigger('mouseenter');
        }
    },

    onBottomControlTriger(self, control) {
        if (self.hasClass('inactive')) {
            if (control == 'prev') {
                clothSwitcher.config.bottomSelector.first().trigger('mouseenter');
            } else {
                clothSwitcher.config.bottomSelector.last().trigger('mouseenter');
            }
        } else {
            clothSwitcher.config.bottomNthSelector.trigger('mouseenter');
        }
    },

    onHoverOut: function() {
        // var selectedImage = clothSwitcher.config.basePreviewImage.attr('src')
        // clothSwitcher.config.selectedPreviewImage.attr('src', selectedImage);
    },
    
    // bottom style option - step 1 - top
    topOnHoverIn: function() {
        clothSwitcher.eventTopEffect($(this), 1);
    },

    topSelectorClicked: function() {
        console.log($(this).attr('id'))
        clothSwitcher.eventTopEffect($(this), 2);
    },

    eventTopEffect: function(_self, _event) {
        clothSwitcher.config.topSelector.removeClass('selected');
        _self.addClass('selected');

        var selectedImage = _self.find('img');
        var selectedTopStyle = {
            src: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (selectedTopStyle) {
            clothSwitcher.setTopStyle(selectedTopStyle);
            clothSwitcher.populateStyle(_event);
        }

        if (clothSwitcher.getTopStyle() && clothSwitcher.getBottomStyle()) {
            clothSwitcher.config.actionButton.prop('disabled', false);
        }
    },

    setTopStyle: function(selectedTopStyle) {
        clothSwitcher.config.currentTopStyle = selectedTopStyle;
    },

    getTopStyle: function () {
        return clothSwitcher.config.currentTopStyle;
    },

    // bottom style option - step 1 - bottom
    bottomOnHoverIn: function() {
        clothSwitcher.eventBottomEffect($(this), 1);
    },

    bottomSelectorClicked: function() {
        clothSwitcher.eventBottomEffect($(this), 2);
    },

    eventBottomEffect: function(_self, _event) {
        clothSwitcher.config.bottomSelector.removeClass('selected');
        _self.addClass('selected');

        var selectedImage = _self.find('img');
        var selectedTopStyle = {
            src: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (selectedTopStyle) {
            clothSwitcher.setBottomStyle(selectedTopStyle);
            clothSwitcher.populateStyle(_event);
        }
        
        if (clothSwitcher.getTopStyle() && clothSwitcher.getBottomStyle()) {
            clothSwitcher.config.actionButton.prop('disabled', false);
        }
        
    },

    setBottomStyle: function(selectedBottomStyle) {
        clothSwitcher.config.currentBottomStyle = selectedBottomStyle;
    },

    getBottomStyle: function () {
        return clothSwitcher.config.currentBottomStyle;
    },

    // print style options - step 2
    printOnHoverIn: function() {
        clothSwitcher.eventPrintEffect($(this), 1);
    },

    printSelectorClicked: function() {
        clothSwitcher.eventPrintEffect($(this), 2);
    },

    eventPrintEffect: function(_self, _event) {
        clothSwitcher.config.printSelector.removeClass('selected');
        _self.addClass('selected');

        var selectedImage = _self.find('img');
        var selectedPrintStyle = {
            src: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (selectedPrintStyle) {
            clothSwitcher.setPrintStyle(selectedPrintStyle);
            clothSwitcher.populateStyle(_event);
        }
        
        if (clothSwitcher.getPrintStyle()) {
            clothSwitcher.config.actionButton.prop('disabled', false);
        }
    },

    setPrintStyle: function(selectedPrintStyle) {
        clothSwitcher.config.currentPrintStyle = selectedPrintStyle;
    },

    getPrintStyle: function () {
        return clothSwitcher.config.currentPrintStyle;
    },

    // belt color options - stpe 3
    beltOnHoverIn: function() {
        clothSwitcher.eventbeltEffect($(this), 1);
    },

    beltColorSelectorClicked: function() {
        clothSwitcher.eventbeltEffect($(this), 2);
    },

    eventbeltEffect: function(_self, _event) {
        clothSwitcher.config.colorSelector.removeClass('selected');
        _self.addClass('selected');

        var selectedImage = _self.find('img');
        var selectedBeltColorStyle = {
            src: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (selectedBeltColorStyle) {
            clothSwitcher.setBeltColorStyle(selectedBeltColorStyle);
            clothSwitcher.populateStyle(_event);
        }
        
        if (clothSwitcher.getBeltColorStyle()) {
            clothSwitcher.config.actionButton.prop('disabled', false);
        }
    },

    setBeltColorStyle: function(selectedBeltColorStyle) {
        clothSwitcher.config.currentBeltStyle = selectedBeltColorStyle;
    },

    getBeltColorStyle: function () {
        return clothSwitcher.config.currentBeltStyle;
    },

    // for reset functionality to set images in default
    setDefault: function() {
        clothSwitcher.config.actionButton.prop('disabled', true);
        clothSwitcher.setPrintStyle(null);
        clothSwitcher.populateStyle();
    },

    // populate selected styles
    populateStyle: function(cb) {
        var previewSrc = INITIAL_IMAGE;
        if (clothSwitcher.getTopStyle()) {
            switch(clothSwitcher.getTopStyle().id) {
                case 'T2':
                    previewSrc = T2_B1_IMAGE; // T2_IMAGE;
                    if (clothSwitcher.getBottomStyle()) {
                        switch(clothSwitcher.getBottomStyle().id) {
                            case 'B2':
                                previewSrc = T2_B2_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T2_B2_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B2_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B2_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T2_B2_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B2_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B2_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T2_B2_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B2_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B2_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                            break;
                            case 'B3':
                                previewSrc = T2_B3_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T2_B3_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B3_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B3_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T2_B3_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B3_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B3_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T2_B3_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B3_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B3_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                            break;
                            default: // B1
                                previewSrc = T2_B1_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T2_B1_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B1_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B1_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T2_B1_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B1_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B1_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T2_B1_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T2_B1_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T2_B1_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                        }
                    }
                break;
                case 'T3':
                    previewSrc = T3_B1_IMAGE; // T3_IMAGE;
                    if (clothSwitcher.getBottomStyle()) {
                        switch(clothSwitcher.getBottomStyle().id) {
                            case 'B2':
                                previewSrc = T3_B2_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T3_B2_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B2_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B2_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T3_B2_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B2_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B2_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T3_B2_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B2_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B2_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                            break;
                            case 'B3':
                                previewSrc = T3_B3_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T3_B3_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B3_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B3_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T3_B3_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B3_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B3_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T3_B3_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B3_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B3_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                            break;
                            default: // B1
                                previewSrc = T3_B1_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T3_B1_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B1_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B1_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T3_B1_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B1_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B1_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T3_B1_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T3_B1_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T3_B1_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                        }
                    }
                break;
                default: // T1
                    previewSrc = T1_B1_IMAGE; //  T1_IMAGE;
                    if (clothSwitcher.getBottomStyle()) {  
                        switch(clothSwitcher.getBottomStyle().id) {
                            case 'B2':
                                previewSrc = T1_B2_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T1_B2_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B2_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B2_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T1_B2_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B2_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B2_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T1_B2_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B2_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B2_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                            break;
                            case 'B3':
                                previewSrc = T1_B3_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T1_B3_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B3_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B3_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T1_B3_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B3_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B3_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T1_B3_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B3_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B3_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                            break;
                            default: // B1
                                previewSrc = T1_B1_IMAGE;
                                if (clothSwitcher.getPrintStyle()) {
                                    switch(clothSwitcher.getPrintStyle().id) {
                                        case 'P2':
                                            previewSrc = T1_B1_P2_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B1_P2_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B1_P2_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        case 'P3':
                                            previewSrc = T1_B1_P3_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B1_P3_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B1_P3_BR_IMAGE;
                                                }
                                            }
                                        break;
                                        default: // P1
                                            previewSrc = T1_B1_P1_IMAGE;
                                            if (clothSwitcher.getBeltColorStyle()) {
                                                switch(clothSwitcher.getBeltColorStyle().id) {
                                                    case 'BG':
                                                        previewSrc = T1_B1_P1_BG_IMAGE;
                                                    break; 
                                                    default: // BR
                                                        previewSrc = T1_B1_P1_BR_IMAGE;
                                                }
                                            }
                                    }
                                }
                        }
                    }
            }
        }

        if (cb === 1) {
            clothSwitcher.config.selectedPreviewImage.attr('src', previewSrc);
        } else {
            clothSwitcher.config.basePreviewImage.attr('src', previewSrc);
        }
    }
};
 
$( document ).ready(
    clothSwitcher.init
);